#!/bin/bash

clear

if test -z "$(type -p)"
then 
echo bash 
else 
echo "Don't run this with Shell i.e. SH use Bash please ./script" 
exit 99
fi

dir="$(dirname "$0")"
#INSTALLS="$dir/${HOSTNAME}-install.list"
#REMOVES="$dir/${HOSTNAME}-remove.list"

INSTALLS="$(pwd)/$(hostname -f)-install.list"
REMOVES="$(pwd)/$(hostname -f)-remove.list"

if [ ! -f $INSTALLS ]; then
    echo $INSTALLS 
	echo "File not found! Please create file."
	read -rsp $'Press enter to continue...\n'
	exit
fi

if [ ! -f $REMOVES ]; then
    echo $REMOVES
	echo "File not found! Please create file."
	read -rsp $'Press enter to continue...\n'
	exit
fi

echo "We will install all packages from the following file" 
echo $INSTALLS
echo "We will remove all packages from the following file"
echo $REMOVES

read -p "Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY = ^[Yy]$ ]]
then
echo   "HEre we GO"
# Update
sudo apt-get -y update

#exit 99

# Install Applications
# Feel free to change the contents of 'install.list' in the data folder to whatever suits your preference.
#        sudo apt-get install -y -qq --no-install-recommends $(cat $INSTALLS)
#        sudo apt-get --force-yes -y install $(cat $INSTALLS
sudo apt-get -y install $(cat $INSTALLS)

# Remove Applications
# Feel free to change the contents of 'remove.list' in the data folder to whatever suits your preference.
sudo apt-get remove -y $(cat $REMOVES)

# Install Ubuntu Restricted Extras
#    sudo apt-get install -y -qq ubuntu-restricted-extras

# Remove Orphaned Packages
sudo apt-get autoremove -y

# Clean Apt Package Cache
sudo apt-get clean

# Update
sudo apt-get -y update

# Upgrade
sudo apt-get -y upgrade

else
  exit 99
fi

exit 99

